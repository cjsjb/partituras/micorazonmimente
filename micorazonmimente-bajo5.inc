\context Staff = "bajo" <<
	\set Staff.instrumentName = "Bajo"
	\set Staff.shortInstrumentName = "B."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "bajo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "bass_8"
		\key d \major

		\bajo

		\bar "|."
	} % Voice
>> % Staff (final)

\context TabStaff = "tabbajo" <<
	\clef moderntab
	\set Staff.stringTunings = \stringTuning <b,,, e,, a,, d, g,>
	%\set Staff.stringTunings = \stringTuning <e,, a,, d, g,>
%	\set Staff.instrumentName = \markup \column {
%		\line { \circle {1} "= G " }
%		\line { \circle {2} "= D " }
%		\line { \circle {3} "= A " }
%		\line { \circle {4} "= e " }
%	}
	\override Stem #'transparent = ##t
	\override Stem #'length = #0
	\override Beam #'transparent = ##t
	\set autoBeaming = ##f
	\override Staff.Rest #'break-visibility = #all-invisible

	\bajo
>>
