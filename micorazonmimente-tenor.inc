\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key d \major

		R1  |
		r2 r4 a,  |
		fis 16 g fis 8 ~ fis 2 g 4  |
		g 8 fis 4. r4 fis  |
%% 5
		g 8 g 4. r4 g 8 g  |
		a 4 ( b cis' ) r8 cis  |
		fis 2. g 4  |
		fis 2 ~ fis 8 r fis fis  |
		a 1 ~  |
%% 10
		a 2 r4 a  |
		b 2. b 4  |
		b 8 a 2 r8 r a  |
		d' 4. d' 8 cis' 4. cis' 8  |
		cis' 8 b 2 r8 r b  |
%% 15
		b 2 b 8 d' 4 cis' 8 ~  |
		cis' 4 r8 a d' 4. cis' 8  |
		a 1  |
		r2 r4 r8 b  |
		b 2 b 8 d' 4 cis' 8 ~  |
%% 20
		cis' 4 r8 a d' 4. cis' 8  |
		d' 1 ~  |
		d' 1  |
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Mi co -- ra -- zón, mi men -- te,
		mi cuer -- po, y mi ser __
		te doy, Se -- ñor, __ tó -- ma -- lo. __

		Te doy mi cuer -- po,
		un sa -- cri -- fi -- cio vi -- vo,
		Se -- ñor, tó -- ma -- lo, __ a -- cép -- ta -- lo.

		Se -- ñor, tó -- ma -- lo, __ a -- cép -- ta -- lo. __		
	}
>>
