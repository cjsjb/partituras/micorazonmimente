\context ChordNames
	\chords {
	\set majorSevenSymbol = \markup { "maj7" }
	\set chordChanges = ##t

	% intro
	d1 d1

	% mi corazon...
	d1 b1:m g2 e2:m a1
	d1 b1:m g2 e2:m a1

	% te doy...
	g1 a1 d2 d2/cis
	b1:m g2 e2:m a1
	d1 b1:m g2 e2:m a1
	g1
	d1
	d1
	}
