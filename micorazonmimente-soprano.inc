\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key d \major

		R1  |
		r2 r4 a  |
		d' 16 e' d' 8 ~ d' 2 e' 4  |
		e' 8 d' 4. r4 d'  |
%% 5
		d' 8 b 4. r4 b 8 b  |
		a 4 ( b cis' ) r8 a  |
		d' 2. e' 4  |
		d' 2 ~ d' 8 r d' d'  |
		a' 1 ~  |
%% 10
		a' 2 r4 fis'  |
		g' 2. g' 4  |
		g' 8 e' 2 r8 r a  |
		fis' 4. g' 8 fis' 4. e' 8  |
		e' 8 d' 2 r8 r d'  |
%% 15
		d' 2 d' 8 fis' 4 e' 8 ~  |
		e' 4 r8 a fis' 4. e' 8  |
		d' 1  |
		r2 r4 r8 d'  |
		d' 2 d' 8 fis' 4 e' 8 ~  |
%% 20
		e' 4 r8 a fis' 4. e' 8  |
		g' 1 (  |
		fis' 1 )  |
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Mi co -- ra -- zón, mi men -- te,
		mi cuer -- po, y mi ser __
		te doy, Se -- ñor, __ tó -- ma -- lo. __

		Te doy mi cuer -- po,
		un sa -- cri -- fi -- cio vi -- vo,
		Se -- ñor, tó -- ma -- lo, __ a -- cép -- ta -- lo.

		Se -- ñor, tó -- ma -- lo, __ a -- cép -- ta -- lo. __		
	}
>>
