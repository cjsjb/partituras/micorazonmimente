\context Staff = "mezzosoprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzosoprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key d \major

		R1  |
		r2 r4 a  |
		d' 16 e' d' 8 ~ d' 2 e' 4  |
		e' 8 d' 4. r4 d'  |
%% 5
		d' 8 b 4. r4 b 8 b  |
		a 4 ( b cis' ) r8 a  |
		d' 2. e' 4  |
		d' 2 ~ d' 8 r d' d'  |
		d' 1 (  |
%% 10
		cis' 2 ) r4 fis'  |
		g' 2. g' 4  |
		g' 8 cis' 2 r8 r a  |
		d' 4. d' 8 cis' 4. cis' 8  |
		cis' 8 b 2 r8 r b  |
%% 15
		b 2 b 8 d' 4 cis' 8 ~  |
		cis' 4 r8 a d' 4. cis' 8  |
		a 1  |
		r2 r4 r8 b  |
		b 2 b 8 d' 4 cis' 8 ~  |
%% 20
		cis' 4 r8 a d' 4. cis' 8  |
		d' 1 ~  |
		d' 1  |
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzosoprano" {
		Mi co -- ra -- zón, mi men -- te,
		mi cuer -- po, y mi ser __
		te doy, Se -- ñor, __ tó -- ma -- lo. __

		Te doy mi cuer -- po,
		un sa -- cri -- fi -- cio vi -- vo,
		Se -- ñor, tó -- ma -- lo, __ a -- cép -- ta -- lo.

		Se -- ñor, tó -- ma -- lo, __ a -- cép -- ta -- lo. __		
	}
>>
